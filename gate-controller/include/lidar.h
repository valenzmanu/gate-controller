#include <Arduino.h>

class Lidar
{
private:
    int check;
    int uart[9];
    const int HEADER = 0x59;
    const byte getData[4] = {0x5A, 0x04, 0x04, 0x62};
    const byte setFrameRate[6] = {0x5A, 0x06, 0x03, 0x00, 0x00, 0x63};
    const byte saveSettings[4] = {0x5A, 0x04, 0x11, 0x6F};

public:
    HardwareSerial *_port;
    int _baudrate;

    Lidar(HardwareSerial *port, int baudrate = 19200);
    float RequestDistance();
};
