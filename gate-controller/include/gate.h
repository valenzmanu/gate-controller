#include <Arduino.h>
#define open 0
#define opening 1
#define close 2
#define closing 3
#define error 6

#ifndef SantaLuisaGate_h
#define SantaLuisaGate_h

class SantaLuisaGate
{
public:
    int _actualState;
    int _lastState;
    int _limitSwtichClosed;
    int _limitSwitchOpen;
    int _limitSwitchClosedPin;
    int _limitSwitchOpenPin;
    int _limitSwitchClosedPinGnd;
    int _limitSwitchOpenPinGnd;
    int _motor;
    int _motorPin;
    float _safeDist;
    SantaLuisaGate(int limitSwitchClosedPin, int limitSwitchClosedPinGnd, int limitSwitchOpenPin, int limitSwitchOpenPinGnd, int motorPin, float safeDist = 300, int actualState = close);
    void ReadSensors();
    int FSM(int controlSignal);
    int Move(int controlSignal = 1);
    int ChangedState();
};

#endif