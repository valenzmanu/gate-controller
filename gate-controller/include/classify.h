#include <Arduino.h>
#include "Filters.h"

/*---------------------------------------------------------
     Clasification FSM Variables
 
 FSM stands for Final State Machine. The clasification
 algorithm is based on a FSM. D1, D2 ... D4 and C1, C2
 ... C4 the FSM states. Input defines are explained in
 the clasification section below. Please refer to README
 for more details
---------------------------------------------------------*/
//Inputs
#define nullInput 0
#define Up1 1
#define Down1 2
#define Up2 3
#define Down2 4

//States
#define Initial 0
#define D1 1
#define D2 2
#define D3 3
#define D4 4
#define C1 5
#define C2 6
#define C3 7
#define C4 8

//Outputs
#define NOTHING 99
#define IN 150
#define OUT 151
#define VEHICLE_PASSING 152

//Door posible States
#define DOOR_IS_OPEN 0
#define DOOR_IS_OPENING 1
#define DOOR_IS_CLOSED 2
#define DOOR_IS_CLOSING 3
#define DOOR_ERROR 6

//Functions
int classifySignals(float dist1, float dist2, float safeDist, int doorState);
void changeClasificationFsmState(int input);
float getVel1();
float getVel2();
int getClasificationFSM_ActualState();