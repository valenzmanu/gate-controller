#include <Arduino.h>

struct debug
{
    unsigned long Time;
    float dist1;
    float vel1;
    float dist2;
    float vel2;
    float trh1_up;
    float trh1_down;
    float trh2_up;
    float trh2_down;
    int classificationFSM;
    int gateFSM;
};

void debuger(HardwareSerial *port, debug debugPackage, int DEBUG_MODE);
