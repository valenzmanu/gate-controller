#include <Arduino.h>
#ifndef SERIAL_COMUNICATION_H
#define SERIAL_COMUNICATION_H

#define gpsbufferSize 100

struct noArgsCmd
{
    char match[100];
};

struct ArgsCmd
{
    char match[100];
    char matchArg1[100];
    char matchArg2[100];
    char matchArg3[100];
    char matchArg4[100];
    char matchArg5[100];
    int arg1;
    int arg2;
    int arg3;
    int arg4;
};

struct generalCmd
{
    String header;
    int arg1;
    int arg2;
    int arg3;
    int arg4;
};

String serialListener(HardwareSerial *port);
generalCmd commandReceiver(HardwareSerial *port, int sendAcknowledgement);
void sendReports(HardwareSerial *port, int enable, int report);

#endif