#include <Arduino.h>
#include "classify.h"

const float lpfCutoff = 2.0;
const float hpfCutoff = 20;
const int hpfGain = 200;

FilterOnePole LPF1(LOWPASS, lpfCutoff);
FilterOnePole LPF2(LOWPASS, lpfCutoff);
FilterOnePole HPF1(HIGHPASS, hpfCutoff);
FilterOnePole HPF2(HIGHPASS, hpfCutoff);

float trh1Up = 30.0;
float trh1Down = -30.0;
float trh2Up = 30.0;
float trh2Down = -30.0;

float vel1;
float vel2;

int clasificationFSM_ActualState = 0;
int car_in_flag = 0;
int car_out_flag = 0;
int already_sent = 0;

int classifySignals(float dist1, float dist2, float safeDist, int doorState)
{
    if (doorState == DOOR_IS_OPEN)
    {
        already_sent = 0;
        /*--------------------------------------
            Filter Data and get Pseudo Velocity
        --------------------------------------*/
        //Remove Noise
        float dist1F;
        float dist2F;
        LPF1.input(dist1);
        LPF2.input(dist2);
        dist1F = LPF1.output();
        dist2F = LPF2.output();

        //Get pseudo velocity
        HPF1.input(dist1F);
        HPF2.input(dist2F);
        vel1 = hpfGain * HPF1.output();
        vel2 = hpfGain * HPF2.output();

        /*--------------------------------------
            Get Clasification Events
        --------------------------------------*/
        //Update FSM state before the next move
        changeClasificationFsmState(nullInput);

        if (vel1 < trh1Down)
        { //Door Down
            //Serial.println("Down1");
            changeClasificationFsmState(Down1);
        }
        else if (vel1 > trh1Up)
        { //Door Up
            //Serial.println("Up1");
            changeClasificationFsmState(Up1);
        }

        if (vel2 < trh2Down)
        { //Car Down
            //Serial.println("Down2");
            changeClasificationFsmState(Down2);
        }
        else if (vel2 > trh2Up)
        { //Car Up
            //Serial.println("Up2");
            changeClasificationFsmState(Up2);
        }

        /*--------------------------------------
            Return Clasification Result
        --------------------------------------*/
        if (car_in_flag)
        {
            car_in_flag = 0;
            return IN;
        }
        else if (car_out_flag)
        {
            car_out_flag = 0;
            return OUT;
        }
    }

    else if (doorState == DOOR_IS_CLOSING)
    {
        if (dist1 < safeDist && !already_sent)
        {
            already_sent = 1;
            return VEHICLE_PASSING;
        }
    }

    return NOTHING;
}

void changeClasificationFsmState(int input)
{
    switch (clasificationFSM_ActualState)
    {
    case Initial:
        //Serial.println("Intial");
        if (input == Down1)
        {

            clasificationFSM_ActualState = D1;
        }
        else if (input == Down2)
        {

            clasificationFSM_ActualState = C1;
        }

        break;

    case D1:
        //Serial.println("D1");
        if (input == Up1)
        {
            clasificationFSM_ActualState = Initial;
        }
        else if (input == Down2)
        {
            clasificationFSM_ActualState = D2;
        }

        break;

    case D2:
        //Serial.println("D2");
        if (input == Up2)
        {
            clasificationFSM_ActualState = D1;
        }
        else if (input == Up1)
        {

            clasificationFSM_ActualState = D3;
        }

        break;

    case D3:
        //Serial.println("D3");
        //door_up_detection_time = millis();
        if (input == Down1)
        {
            clasificationFSM_ActualState = D2;
        }
        else if (input == Up2)
        {
            clasificationFSM_ActualState = D4;
        }

        break;

    case D4:

        //Serial.print("D4: Entró Carro \t Tpass: ");
        //Serial.println(Tpass);
        car_in_flag = 1;

        clasificationFSM_ActualState = Initial;
        break;

    case C1:
        //Serial.println("C1");
        if (input == Up2)
        {
            clasificationFSM_ActualState = Initial;
        }
        else if (input == Down1)
        {
            clasificationFSM_ActualState = C2;
        }

        break;

    case C2:
        //Serial.println("C2");
        if (input == Up1)
        {
            clasificationFSM_ActualState = C1;
        }
        else if (input == Up2)
        {

            clasificationFSM_ActualState = C3;
        }

        break;

    case C3:
        //Serial.println("C3");
        if (input == Down2)
        {
            clasificationFSM_ActualState = C2;
        }
        else if (input == Up1)
        {
            clasificationFSM_ActualState = C4;
        }

        break;

    case C4:
        //Tpass = car_down_detection_time - car_up_detection_time;
        //Serial.print("C4: Salió Carro \t Tpass: ");
        //Serial.println(Tpass);
        car_out_flag = 1;

        clasificationFSM_ActualState = Initial;
        break;

    default:
        break;
    }
}

float getVel1()
{
    return vel1;
}

float getVel2()
{
    return vel1;
}

int getClasificationFSM_ActualState()
{
    return clasificationFSM_ActualState;
}