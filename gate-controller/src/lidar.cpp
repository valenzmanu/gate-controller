#include "lidar.h"

Lidar::Lidar(HardwareSerial *port, int baudrate = 19200)
{
    _port = port;
    _baudrate = baudrate;
    port->begin(baudrate);
}

float Lidar::RequestDistance()
{
    _port->write(getData, sizeof(getData));
    delay(20);
    if (_port->available())
    { //check if serial port has data input
        if (_port->read() == HEADER)
        { //Passess data package frame header 0x59
            uart[0] = HEADER;
            if (_port->read() == HEADER)
            { //Passess data package frame header 0x59
                uart[1] = HEADER;
                for (int i = 2; i < 9; i++)
                { //save data in array
                    uart[i] = _port->read();
                }
                check = uart[0] + uart[1] + uart[2] + uart[3] + uart[4] + uart[5] + uart[6] + uart[7];
                if (uart[8] == (check & 0xff))
                {                                   //verify the received data as per protocol
                    return uart[2] + uart[3] * 256; //Calculate Distance
                }
            }
        }
    }
    return 0;
}