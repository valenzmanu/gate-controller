#include <Arduino.h>
#include "serial_comunication.h"
#include "Regexp.h"

String serialListener(HardwareSerial *port)
{ //Listen to the given serial port
    int bufIndex = 0;
    char gpsBuffer[gpsbufferSize];
    memset(gpsBuffer, '\0', 100);
    String cmdReceived = "";
    while (port->available() && bufIndex < 100)
    {
        gpsBuffer[bufIndex] = port->read();
        delay(10);
        bufIndex++;
    }
    for (int i = 0; i < gpsbufferSize; i++)
    {
        if (gpsBuffer[i] != '\0')
        {
            cmdReceived.concat(String(gpsBuffer[i]));
        }
    }
    return cmdReceived;
}

generalCmd commandReceiver(HardwareSerial *port, int sendAcknowledgement)
{
    /*----------------------------------------------
        Receive command
    ----------------------------------------------*/

    String cmd = "";
    cmd = serialListener(port);
    cmd.trim();
    char command[gpsbufferSize];
    cmd.toCharArray(command, gpsbufferSize);

    /*----------------------------------------------
        Define commands to match with
    ----------------------------------------------*/

    //Match Object
    MatchState parseCommand;
    parseCommand.Target(command);

    //Commands and arguments
    noArgsCmd cmd_STATE = {"^#STATE$"};
    noArgsCmd cmd_OPEN_DOOR = {"^#OPEN_DOOR$"};
    noArgsCmd cmd_CLOSE_DOOR = {"^#CLOSE_DOOR$"};
    noArgsCmd cmd_RESTART = {"^#RESTART$"};
    noArgsCmd cmd_FACTORY_RESET = {"^#FACTORY_RESET$"};

    ArgsCmd cmd_TRH = {"^#TRH:",
                       "d_up=%d+;",
                       "d_down=%-%d+;",
                       "c_up=%d+;",
                       "c_down=%-%d+;",
                       "?;"};

    ArgsCmd cmd_SAFE_DIST = {"^#SAFE_DIST:",
                             "%d+;"};

    ArgsCmd cmd_T_GATE = {"^#T_GATE:",
                          "%d+;",
                          "?;"};

    ArgsCmd cmd_DEBUG = {"#DEBUG:",
                         "DEBUG=%d;",
                         "DEBUG_MODE=%d;"};

    /*----------------------------------------------
        Parse command received
    ----------------------------------------------*/
    generalCmd generalCommand;
    if (parseCommand.Match(cmd_STATE.match) > 0)
    {
        Serial.println("STATE");
        if (sendAcknowledgement)
        {
            port->println("AT$SACK");
        }
    }
    if (parseCommand.Match(cmd_OPEN_DOOR.match) > 0)
    {
        generalCommand.header = "OPEN";
        if (sendAcknowledgement)
        {
            port->println("AT$SACK");
        }
    }
    if (parseCommand.Match(cmd_CLOSE_DOOR.match) > 0)
    {
        generalCommand.header = "CLOSE";
        if (sendAcknowledgement)
        {
            port->println("AT$SACK");
        }
    }
    return generalCommand;
}

void sendReports(HardwareSerial *port, int enable, int report)
{ //Send the given report through the given serial port
    if (enable)
    {
        port->println("AT$FORM=0,@T,0,\"%VB\"");               // Change the reading format of the data received by the GPS to TAB
        port->println("AT$FUNC=\"VRBL\",0," + String(report)); // Send Event
        port->println("AT$GPOS=2,0");
    }
}