#include <Arduino.h>
#include "gate.h"

#define openingGate 10
#define closingGate 11
#define notMoving 12

/*---------------------------------------------------------------------------------
    Class Constructor
---------------------------------------------------------------------------------*/
SantaLuisaGate::SantaLuisaGate(int limitSwitchClosedPin, int limitSwitchClosedPinGnd, int limitSwitchOpenPin, int limitSwitchOpenPinGnd, int motorPin, float safeDist, int actualState)
{
    _actualState = actualState;
    _lastState = actualState;
    _limitSwitchClosedPin = limitSwitchClosedPin;
    _limitSwitchOpenPin = limitSwitchOpenPin;
    _limitSwitchClosedPinGnd = limitSwitchClosedPinGnd;
    _limitSwitchOpenPinGnd = limitSwitchOpenPinGnd;
    _motorPin = motorPin;
    _safeDist = safeDist;

    //Pin Configuration
    pinMode(_limitSwitchClosedPinGnd, OUTPUT);
    pinMode(_limitSwitchOpenPinGnd, OUTPUT);
    pinMode(_motorPin, OUTPUT);
    pinMode(_limitSwitchClosedPin, INPUT_PULLUP);
    pinMode(_limitSwitchOpenPin, INPUT_PULLUP);

    //Set GND to Sensors
    digitalWrite(_limitSwitchClosedPinGnd, LOW);
    digitalWrite(_limitSwitchOpenPinGnd, LOW);
}

/*---------------------------------------------------------------------------------
    Read Sensors connected to the public internal atributes _limitSwitchClosedPin
    and _limitSwitchOpenPin. The state of those sensors are used by the FSM method
    to determine the gate state.
---------------------------------------------------------------------------------*/
void SantaLuisaGate::ReadSensors()
{
    _limitSwtichClosed = !digitalRead(_limitSwitchClosedPin);
    _limitSwitchOpen = !digitalRead(_limitSwitchOpenPin);
}

/*---------------------------------------------------------------------------------
    Update the gate state reading sensor state using the actual state of the
    public internal atributes _limitSwtichClosed , _limitSwitchOpen and the 
    parameter controlSignal, which is the signal of an external controller.
    If no external controller connected use FSM(0). 
    
    The updated gate state is returned as an int:
    - open 0
    - opening 1
    - close 2
    - closing 3
    - error 6
---------------------------------------------------------------------------------*/
int SantaLuisaGate::FSM(int controlSignal)
{
    switch (_actualState)
    {
    case close:
        if (!_limitSwtichClosed && !_limitSwitchOpen)
        {
            _actualState = opening;
            return _actualState;
        }
        else if (!_limitSwtichClosed && _limitSwitchOpen)
        {
            _actualState = open;
            return _actualState;
        }
        else
        {
            return close;
        }

        break;

    case opening:
        if (_limitSwtichClosed)
        {
            _actualState = close;
            return _actualState;
        }
        else if (!_limitSwtichClosed && !_limitSwitchOpen)
        {
            _actualState = opening;
            return _actualState;
        }
        else if (!_limitSwtichClosed && _limitSwitchOpen)
        {
            _actualState = open;
            return _actualState;
        }
        else
        {
            //_actualState = error;
            return error;
        }

        break;

    case open:
        if (_limitSwtichClosed)
        {
            _actualState = close;
            return _actualState;
        }
        else if (!_limitSwtichClosed && !_limitSwitchOpen)
        {
            _actualState = closing;
            return _actualState;
        }
        else if (!_limitSwtichClosed && _limitSwitchOpen)
        {
            _actualState = open;
            return _actualState;
        }
        else
        {
            return error;
        }

        break;

    case closing:
        if (_limitSwtichClosed)
        {
            _actualState = close;
            return _actualState;
        }
        else if (!_limitSwtichClosed && _limitSwitchOpen)
        {
            _actualState = open;
            return _actualState;
        }
        else if (controlSignal)
        {
            _actualState = opening;
            return _actualState;
        }
        else
        {
            _actualState = closing;
            return _actualState;
        }

        break;

    default:
        return close;
        break;
    }
}

/*---------------------------------------------------------------------------------
    Switch the gate state by sending a 1 second pulse to the public internal
    parameter _motorPin. Optional external enable is used as a parameter.
    
    A movement confirmation is returned as an int:
    - openingGate 10
    - closingGate 11
    - notMoving 12
---------------------------------------------------------------------------------*/
int SantaLuisaGate::Move(int controlSignal)
{
    if (controlSignal)
    {
        if (_actualState == open || _actualState == close || _actualState == closing)
        {
            digitalWrite(_motorPin, HIGH);
            delay(1000);
            digitalWrite(_motorPin, LOW);
            if (_actualState == open)
            {
                return closingGate;
            }
            else if (_actualState == close)
            {
                return openingGate;
            }
            else
            {
                return notMoving;
            }
        }
    }
    else
    {
        return notMoving;
    }
    return 0;
}

/*---------------------------------------------------------------------------------
    Check if the gate changed from closed to open and viceversa. This could be
    used as a condition to send a report.
    
    The action is returned as an int:
    - open 0: closed -> open
    - close 2: open -> closed
    - notMoving 12
---------------------------------------------------------------------------------*/
int SantaLuisaGate::ChangedState()
{
    if (_lastState == open && _actualState == close)
    {
        _lastState = close;
        return close;
    }
    else if (_lastState == close && _actualState == open)
    {
        _lastState = open;
        return open;
    }
    return notMoving;
}