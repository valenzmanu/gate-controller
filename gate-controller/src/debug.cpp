#include "debug.h"

void debuger(HardwareSerial *port, debug debugPackage, int DEBUG_MODE)
{

    switch (DEBUG_MODE)
    {
    case 1:
        //Lidar Distances and Velocities
        port->print(debugPackage.dist1);
        port->print(", ");
        port->print(debugPackage.vel1);
        port->print(", ");
        port->print(debugPackage.dist2);
        port->print(", ");
        port->println(debugPackage.vel2);
        break;
    case 2:
        //To use serial monitor tool
        port->print("#Tool: ");
        port->print(debugPackage.Time);
        port->print(", ");
        port->print(debugPackage.dist1);
        port->print(", ");
        port->print(debugPackage.vel1);
        port->print(", ");
        port->print(debugPackage.dist2);
        port->print(", ");
        port->print(debugPackage.vel2);
        port->print(", ");
        port->print(debugPackage.trh1_up);
        port->print(", ");
        port->print(debugPackage.trh1_down);
        port->print(", ");
        port->print(debugPackage.trh2_up);
        port->print(", ");
        port->print(debugPackage.trh2_down);
        port->print(", ");
        port->print(debugPackage.classificationFSM);
        port->print(", ");
        port->println(debugPackage.gateFSM);
        break;

    default:
        break;
    }
}