/*************************************************************/
/*  StarTrack GPS Development                                */
/*  Author: Manuel Valenzuela                                */
/*  Date: 10/10/2019                                         */
/*  Microcontroller: atmega2560                              */
/*  Board:  Arduino Mega 2560 R3                             */
/*************************************************************/

#include <Arduino.h>
#include "gate.h"
#include "lidar.h"
#include "classify.h"
#include "serial_comunication.h"
#include "debug.h"

/*------------------------------------------------------------
  Sensor's Pin Numbers
------------------------------------------------------------*/

#define lsOpenPin 35
#define lsClosedPin 43
#define lsOpenPinGnd 31
#define lsClosedPinGnd 39
#define controllerPin 11

/*------------------------------------------------------------
  Serial Ports Used
------------------------------------------------------------*/

#define pcPort Serial
#define lidar1Port Serial1
#define lidar2Port Serial2
#define gpsPort Serial3

/*------------------------------------------------------------
  Debug Variables
------------------------------------------------------------*/
int DEBUG_ENABLE = 0;
int DEBUG_MODE = 2;
debug debugPackage;

/*------------------------------------------------------------
  Class Instances
------------------------------------------------------------*/

//Gate Instance
SantaLuisaGate gate = SantaLuisaGate(lsClosedPin, lsClosedPinGnd, lsOpenPin, lsOpenPinGnd, controllerPin, 10.0);

//Lidar Sensors Instances
Lidar lidar1 = Lidar(&lidar1Port);
Lidar lidar2 = Lidar(&lidar2Port);

/*------------------------------------------------------------
  Internal Functions Declaration
------------------------------------------------------------*/

void doorReports(HardwareSerial *port, int rep);
void vehicleReports(HardwareSerial *port, int vehicleStatus);
void gpsDoorController(generalCmd command);
void internalController(int vehicleStatus);
void ledIndicator(int door, int vehicle);
void debugProgram();

/*------------------------------------------------------------
  Internal Functions Implementation
------------------------------------------------------------*/

void gpsDoorController(generalCmd command)
{ //Moves the gate according to given command
  if (command.header == "OPEN")
  {
    if (gate._actualState == 2)
    {
      gate.Move(1);
    }
  }
  else if (command.header == "CLOSE")
  {
    if (gate._actualState == 0)
    {
      gate.Move(1);
    }
  }
}

void internalController(int vehicleStatus)
{ //Stops the gate if a vehicle is in between
  if (vehicleStatus == 152)
  {
    gate.Move(1);
  }
}

void doorReports(HardwareSerial *port, int rep)
{ //Send the given door report through given port

  switch (rep)
  {
  case 0:
    Serial.println("Abrió Puerta");
    sendReports(port, 1, 8);
    break;
  case 2:
    Serial.println("Cerró Puerta");
    sendReports(port, 1, 9);
    break;

  default:
    break;
  }
}

void vehicleReports(HardwareSerial *port, int vehicleStatus)
{ //Send the given vehicle report through given port
  switch (vehicleStatus)
  {
  case 150:
    Serial.println("Entró Carro");
    sendReports(port, 1, 150);
    break;

  case 151:
    Serial.println("Salió Carro");
    sendReports(port, 1, 151);
    break;

  case 152:
    Serial.println("Puerta Detenida");
    sendReports(port, 1, 154);
    break;

  default:
    break;
  }
}

void debugProgram(HardwareSerial *port)
{ //Send main variables through given port
  if (DEBUG_ENABLE)
  {
    debugPackage.Time = millis();
    debugPackage.dist1 = lidar1.RequestDistance();
    debugPackage.dist2 = lidar2.RequestDistance();
    debugPackage.vel1 = getVel1();
    debugPackage.vel2 = getVel2();
    debugPackage.classificationFSM = getClasificationFSM_ActualState();
    debugPackage.gateFSM = gate._actualState;
    debuger(port, debugPackage, DEBUG_MODE);
  }
}

void ledIndicator(int door, int vehicle)
{
  switch (door)
  {
  case 0:
    digitalWrite(11, HIGH);
    delay(100);
    digitalWrite(11, LOW);
    delay(100);
    digitalWrite(11, HIGH);
    delay(100);
    digitalWrite(11, LOW);
    break;
  case 2:
    digitalWrite(12, HIGH);
    delay(100);
    digitalWrite(12, LOW);
    delay(100);
    digitalWrite(12, HIGH);
    delay(100);
    digitalWrite(12, LOW);
    break;
  default:
    break;
  }
  switch (vehicle)
  {
  case 150:
    digitalWrite(11, HIGH);
    delay(100);
    digitalWrite(12, HIGH);
    delay(100);
    digitalWrite(11, LOW);
    delay(100);
    digitalWrite(12, LOW);
    break;

  case 151:
    digitalWrite(12, HIGH);
    delay(100);
    digitalWrite(11, HIGH);
    delay(100);
    digitalWrite(12, LOW);
    delay(100);
    digitalWrite(11, LOW);
    break;

  case 152:
    digitalWrite(12, HIGH);
    digitalWrite(11, HIGH);
    delay(200);
    digitalWrite(12, LOW);
    digitalWrite(11, LOW);
    delay(200);
    digitalWrite(12, HIGH);
    digitalWrite(11, HIGH);
    delay(200);
    digitalWrite(12, LOW);
    digitalWrite(11, LOW);
    break;

  default:
    break;
  }
}

/*------------------------------------------------------------
  Setup and Main Loop
------------------------------------------------------------*/

void setup()
{
  //Pin Mode Init
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);

  //Serial Port Init
  pcPort.begin(19200);
  gpsPort.begin(19200);
}

void loop()
{

  //Update Door State
  int report;
  gate.ReadSensors();
  gate.FSM(0);
  report = gate.ChangedState();

  //Update Lidar Info
  float distance1;
  float distance2;
  distance1 = lidar1.RequestDistance();
  distance2 = lidar2.RequestDistance();

  //Determine if a vehicle goes in or out
  int vehicleStatus;
  vehicleStatus = classifySignals(distance1, distance2, gate._safeDist, gate._actualState);

  /* 
  TODO:
  4.  Usar EEPROM para guardar 
      las configuraciones.
  */

  //Serial Control
  generalCmd serialCommand;
  generalCmd gpsCommand;
  serialCommand = commandReceiver(&pcPort, 0);
  gpsCommand = commandReceiver(&gpsPort, 1);
  gpsDoorController(serialCommand);
  gpsDoorController(gpsCommand);

  //Internal control
  internalController(vehicleStatus);

  //Reports
  doorReports(&gpsPort, report);
  vehicleReports(&gpsPort, vehicleStatus);

  //Indicator Leds
  ledIndicator(report, vehicleStatus);

  //Debug
  debugProgram(&pcPort);
}
